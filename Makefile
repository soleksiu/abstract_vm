#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: soleksiu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/08 16:02:30 by soleksiu          #+#    #+#              #
#    Updated: 2018/07/08 16:02:35 by soleksiu         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

SRCDIR = source_files

OBJDIR = object_files

SRC = main.cpp AbstractVM.cpp Lexer.cpp Parser.cpp OperandFactory.cpp Operand.cpp
	  
OBJ = $(SRC:.cpp=.o)

HEADERS = includes/AbstractVM.hpp includes/DataModels.hpp includes/IOperand.hpp includes/Lexer.hpp includes/Operand.hpp includes/OperandFactory.hpp includes/Parser.hpp   

OBJS = $(addprefix $(OBJDIR)/,$(SRC:.cpp=.o))

NAME = avm

WRN =  -Wall -Wextra -Werror

COMPILER = clang++ -std=c++11

STORAGE_NAME = DRIVE

all: objdir $(NAME)

$(NAME): objdir $(OBJS) $(HEADERS)
	$(COMPILER) $(WRN) -I includes $(OBJS) -o $(NAME) 

objdir: 
	@mkdir -p $(OBJDIR)

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(COMPILER) $(WRN) -I includes -c $< -o $@  

test: all
	@./$(NAME) test/pdf_ex1

backup: fclean
	@mkdir -p ~/Desktop/backup_$(NAME)
	@cp -r * ~/Desktop/backup_$(NAME)

clean:
	@rm -rf $(OBJS)

fclean: clean
	@find . -name "*~" -delete -or -name "#*#" -delete -or -name ".DS_Store" -delete -or -name "a.out" -delete
	@rm -rf $(NAME)
	@rm -rf $(OBJDIR)

re: fclean all