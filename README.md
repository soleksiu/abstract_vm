# Abstract VM.
### Language of development: C++.
This is a learning project of Unit Factory coding school, which using
'ecole 42' franchise and same learning programm as the network of innovative
schools across the world in such countries as USA, France, Netherlands.

 The purpose of this project is to create a simple virtual machine that can
interpret programs written in a basic assembly language.

 To make life funnier, using the external libraries or frameworks was
forbidden or very limited for student projects. Libft included in repository
was developed by author as another lerning project in school 42. Available
external functions for this project is: open, read, write, close, malloc, free,
perror, strerror and exit. All additional stuff needed for project realisation
student must done by yourself.

NOTE: Project was developed and tested on macOS High Sierra 10.13.3.
Unfortunately I have no ability to test it on other platforms for now.
## Installation
Use make to compile the project.

```make avm```
## Usage
Programm can get input from cin or file. You can use files from tests repository
or create your own.

Execute with: 
```./avm```
or
```./avm tests/filename```
## Interface screenshots

### Help realisation
![Screenshot](screenshots/avm_help.png)
### Supported instructions
![Screenshot](screenshots/avm_instructions.png)
### Errors handling
![Screenshot](screenshots/avm_lexer_errors.png)
![Screenshot](screenshots/avm_types_overflow.png)
### Execution with correct input
![Screenshot](screenshots/avm_simple_operations.png)
