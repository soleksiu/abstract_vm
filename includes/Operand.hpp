// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Operand.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/01 14:17:08 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/01 14:17:09 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OPERAND_HPP
# define OPERAND_HPP

#include "IOperand.hpp"
#include <iostream>
#include <string>
#include <map>
#include <exception>
#include <limits>
#include <float.h>
#include <cmath>
#include <type_traits>
#include <iomanip>
#include "DataModels.hpp"
#include "AbstractVM.hpp"

template <class Type>
class Operand: public IOperand
{
public:
	Operand() : _rawValue("Default"), _precision(8), _typeId(Int8) {
		this->_operandValue = 42;
	}
	Operand(eOperandType typeId, std::string initValue) : _rawValue(initValue), _typeId(typeId){
		std::string checkResult;
		
		checkResult = checkTypeOverflow(initValue);
		if (checkResult != "OK") {
			throw OperandException(checkResult);
		}
		if (typeId == Float) {
			float valueFloat = stof(initValue);

			this->_operandValue = valueFloat;
		}
		else if (typeId == Double) {
			double valueDouble = stod(initValue);
			this->_operandValue = valueDouble;

		}
		else 
		{
			if (initValue.find('.') != std::string::npos) {
				throw OperandException("Floating point number in decimal type");
			}
			long long valueLong = stol(initValue); 
			this->_operandValue = valueLong;
		}
		this->_precision = typeId;
	}
	Operand(Operand const &to_copy) {
		this->_operandValue = to_copy->_operandValue;
		this->_precision = to_copy->_precision;
		this->_rawValue = to_copy->_rawValue;
	}
	Operand	&operator=(Operand const &rhs) {
			if (*this != rhs) {
				this->_operandValue = rhs->_operandValue;
				this->_precision = rhs->_precision;
				this->_rawValue = rhs->_rawValue;
			}
	}
	virtual	~Operand() {}

	std::string checkTypeOverflow(std::string initValue) {
		static std::map<eOperandType, std::string> typesStr = 
		{
			{Int8, "int8"},
			{Int16, "int16"},
			{Int32, "int32"},
			{Float, "float"},
			{Double, "double"}
		};
		std::string	curType = typesStr[this->_typeId];
		std::string overflow = "Overflow for " + curType + " type";
		std::string underflow = "Underflow for " + curType + " type";
		if (initValue.length() > 4935)
			return (overflow);
		long double valueToCheck = stold(initValue);
		if (this->_typeId >= Float) {
			valueToCheck = std::fabsl(valueToCheck);
			if (valueToCheck > std::numeric_limits<Type>::max() || valueToCheck < -std::numeric_limits<Type>::max())
				return (overflow);
			else if ((valueToCheck > 0.0 && valueToCheck < 1.0) && valueToCheck < std::numeric_limits<Type>::min())
				return (underflow);
			else
				return ("OK");
		}
		else {
			if (valueToCheck < std::numeric_limits<Type>::min())
				return (this->_typeId < Float ? overflow : underflow);
			else if (valueToCheck > std::numeric_limits<Type>::max())
				return (overflow);
		}
		return ("OK");
	}
	int getPrecision( void ) const override {
		return this->_precision;
	}

	eOperandType getType( void ) const override {
		return this->_typeId;
	}

	IOperand const * operator+(IOperand const & rhs ) const override {
		IOperand const *oper = NULL;
		Operand const &rhsVal = static_cast<Operand  const &>(rhs);
		if (this->_precision != rhsVal._precision) {
			int higherPrecision = this->_precision > rhsVal._precision ? this->_precision : rhsVal._precision;
			eOperandType resultType = this->_precision > rhsVal._precision ? this->_typeId : rhsVal._typeId;
			if (higherPrecision >= Float && this->_precision >= Float && rhsVal._precision >= Float) {
			 	double lValue = stod(this->toString());
			 	double rValue = stod(rhsVal.toString());
				double resFloating = lValue + rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}
			else if (higherPrecision >= Float) {
			 	long long lValue = this->_typeId < Float ? stol(this->toString()) : stol(rhs.toString());
			 	double rValue = this->_typeId >= Float ? stod(this->toString()) : stod(rhsVal.toString());
				double resFloating = static_cast<double>(lValue) + rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}

			else {
				long long resDecimal = stol(this->toString()) + stol(rhsVal.toString());
				oper = this->_factory.createOperand(resultType, std::to_string(resDecimal));
			}
		}
		else {
			if (this->_precision >= Float)
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stold(this->toString()) + stold(rhsVal.toString())));
			else
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stol(this->toString()) + stol(rhsVal.toString())));		}
		return oper;
	}

	IOperand const * operator-( IOperand const & rhs ) const override {
		IOperand const *oper = NULL;
		Operand const &rhsVal = static_cast<Operand  const &>(rhs);
		if (this->_precision != rhsVal._precision) {
			int higherPrecision = this->_precision > rhsVal._precision ? this->_precision : rhsVal._precision;
			eOperandType resultType = this->_precision > rhsVal._precision ? this->_typeId : rhsVal._typeId;
			if (higherPrecision >= Float && this->_precision >= Float && rhsVal._precision >= Float) {
			 	double lValue = stod(this->toString());
			 	double rValue = stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? rValue - lValue : lValue - rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue.erase(operandValue.find_last_not_of('0') + 1, std::string::npos);
				oper = this->_factory.createOperand(Double, operandValue);
			}
			else if (higherPrecision >= Float) {
			 	long long lValue = this->_typeId < Float ? stol(this->toString()) : stol(rhs.toString());
			 	double rValue = this->_typeId >= Float ? stod(this->toString()) : stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? rValue - lValue : lValue - rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue.erase(operandValue.find_last_not_of('0') + 1, std::string::npos);
				oper = this->_factory.createOperand(Double, operandValue);
			}

			else {
				long long resDecimal = stol(this->toString()) - stol(rhsVal.toString());
				oper = this->_factory.createOperand(resultType, std::to_string(resDecimal));
			}
		}
		else {
			if (this->_precision >= Float)
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stold(this->toString()) - stold(rhsVal.toString())));
			else
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stol(this->toString()) - stol(rhsVal.toString())));		}
		return oper;
	}

	IOperand const * operator*( IOperand const & rhs ) const  override {
		IOperand const *oper = NULL;
		Operand const &rhsVal = static_cast<Operand  const &>(rhs);
		if (this->_precision != rhsVal._precision) {
			int higherPrecision = this->_precision > rhsVal._precision ? this->_precision : rhsVal._precision;
			eOperandType resultType = this->_precision > rhsVal._precision ? this->_typeId : rhsVal._typeId;
			if (higherPrecision >= Float && this->_precision >= Float && rhsVal._precision >= Float) {
			 	double lValue = stod(this->toString());
			 	double rValue = stod(rhsVal.toString());
				double resFloating = lValue * rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);;
			}
			else if (higherPrecision >= Float) {
			 	long long lValue = this->_typeId < Float ? stol(this->toString()) : stol(rhs.toString());
			 	double rValue = this->_typeId >= Float ? stod(this->toString()) : stod(rhsVal.toString());
				double resFloating = static_cast<double>(lValue) * rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}

			else {
				long long resDecimal = stol(this->toString()) * stol(rhsVal.toString());
				oper = this->_factory.createOperand(resultType, std::to_string(resDecimal));
			}
		}
		else {
			if (this->_precision >= Float)
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stold(this->toString()) * stold(rhsVal.toString())));
			else
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stol(this->toString()) * stol(rhsVal.toString())));
		}
		return oper;;
	}

	IOperand const * operator/( IOperand const & rhs ) const  override {
		IOperand const *oper = NULL;
		Operand const &rhsVal = static_cast<Operand  const &>(rhs);

		if (!stod(rhsVal._rawValue)) {
			throw OperandException("Divide by zero.");
		}
		else if (this->_precision != rhsVal._precision) {
			int higherPrecision = this->_precision > rhsVal._precision ? this->_precision : rhsVal._precision;
			eOperandType resultType = this->_precision > rhsVal._precision ? this->_typeId : rhsVal._typeId;
			if (higherPrecision >= Float && this->_precision >= Float && rhsVal._precision >= Float) {
			 	double lValue = stod(this->toString());
			 	double rValue = stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? rValue / lValue : lValue / rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}
			else if (higherPrecision >= Float) {

			 	long long lValue = this->_typeId < Float ? stol(this->toString()) : stol(rhsVal.toString());
			 	double rValue = this->_typeId >= Float ? stod(this->toString()) : stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? rValue / lValue : lValue / rValue;
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}

			else {
				long long resDecimal = stol(this->toString()) / stol(rhsVal.toString());
				oper = this->_factory.createOperand(resultType, std::to_string(resDecimal));
			}
		}
		else {
			if (this->_precision >= Float)
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stold(this->toString()) / stold(rhsVal.toString())));
			else
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stol(this->toString()) / stol(rhsVal.toString())));
		}
		
		return oper;
	}

	IOperand const * operator%( IOperand const & rhs ) const  override {
		IOperand const *oper = NULL;
		Operand const &rhsVal = static_cast<Operand  const &>(rhs);

		if (!stol(rhsVal._rawValue)) {
			oper = static_cast<IOperand const *>(this);
			throw OperandException("Modulo division by zero.");
		}
		else if (this->_precision != rhsVal._precision) {
			int higherPrecision = this->_precision > rhsVal._precision ? this->_precision : rhsVal._precision;
			eOperandType resultType = this->_precision > rhsVal._precision ? this->_typeId : rhsVal._typeId;
			if (higherPrecision >= Float && this->_precision >= Float && rhsVal._precision >= Float) {
			 	double lValue = stod(this->toString());
			 	double rValue = stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? std::fmod(rValue, lValue) : std::fmod(lValue, rValue);
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}
			else if (higherPrecision >= Float) {

			 	long long lValue = this->_typeId < Float ? stol(this->toString()) : stol(rhsVal.toString());
			 	double rValue = this->_typeId >= Float ? stod(this->toString()) : stod(rhsVal.toString());
				double resFloating = this->_typeId >= Float ? std::fmod(rValue, lValue) : std::fmod(lValue, rValue);
				std::string operandValue = std::to_string(resFloating);
				operandValue = removeTrailingZeroes(resFloating, operandValue);
				oper = this->_factory.createOperand(Double, operandValue);
			}
			else {
				long long resDecimal = stol(this->toString()) % stol(rhsVal.toString());
				oper = this->_factory.createOperand(resultType, std::to_string(resDecimal));
			}
		}
		else {
			if (this->_precision >= Float)
				oper = this->_factory.createOperand(this->_typeId, std::to_string(std::fmod(stold(this->toString()), stold(rhsVal.toString()))));
			else
				oper = this->_factory.createOperand(this->_typeId, std::to_string(stol(this->toString()) % stol(rhsVal.toString())));
		}
		return oper;
	}
	bool			operator<( IOperand const & rhs ) const  override {
	 	double lValue = stod(this->toString());
	 	double rValue = stod(rhs.toString());
		return (lValue < rValue);
	}

	std::string const & toString( void ) const  override {
		return this->_rawValue;
	}

	class OperandException : public std::exception
	{
	public:
				OperandException(void) : _errorText("Operand error: ") {}
				OperandException(std::string error) {
					std::string colorModMag = AbstractVM::_outputModifiers["magenta"];
					std::string colorModWhite = AbstractVM::_outputModifiers["white"];
					std::string boldModifier = AbstractVM::_outputModifiers["bold"];
					std::string resetModifier = AbstractVM::_outputModifiers["reset"];
					this->_errorText = boldModifier + colorModMag + "Operand error: " + colorModWhite + error + resetModifier;
				}

				OperandException(OperandException  const &to_copy) {
					this->_errorText = to_copy._errorText;
				}
				OperandException &operator=(OperandException const &rhs) {
					if (*this != rhs) {
						this->_errorText = rhs._errorText;
					}
				}
				virtual ~OperandException() throw() {}
				virtual const char * what() const throw() {
					return this->_errorText.c_str();
				}
	private: 
		std::string _errorText;
	};

private:
	std::string		_rawValue;
	int				_precision;
	Type 			_operandValue;
	eOperandType	_typeId;
	OperandFactory	_factory;
	std::string 	removeTrailingZeroes(double origValue, std::string valueStr) const
	{
		std::string cleanedString = valueStr;
		size_t		dotIndex = valueStr.find('.');
		if (origValue > 1)
		{
			if (cleanedString[dotIndex + 1] == '0')
				cleanedString.erase(cleanedString.find_last_not_of('0') + 2, std::string::npos);
			else
				cleanedString.erase(cleanedString.find_last_not_of('0') + 1, std::string::npos);
		}
		return (cleanedString);
	}


};

#endif