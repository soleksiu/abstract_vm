// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OperandFactory.hpp                                 :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/01 12:49:41 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/01 12:49:42 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OPERANDFACTORY_HPP
# define OPERANDFACTORY_HPP

#include "IOperand.hpp"
#include <vector>

class OperandFactory
{
public:
					OperandFactory(void);
					OperandFactory(OperandFactory  const &to_copy);
	OperandFactory	&operator=(OperandFactory const &rhs);
						~OperandFactory();
	IOperand const	*createOperand(eOperandType type, std::string const & value ) const;
	typedef const IOperand*(OperandFactory::*createMethodPtr)(std::string const &) const;

private:
	IOperand const	*createInt8(std::string const & value) const;
	IOperand const	*createInt16(std::string const & value) const;
	IOperand const	*createInt32(std::string const & value) const;
	IOperand const	*createFloat(std::string const & value) const;
	IOperand const	*createDouble(std::string const & value) const;
	std::vector<createMethodPtr> createMethodsVector;	
};

#endif