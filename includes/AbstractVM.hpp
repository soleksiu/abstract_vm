// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AbstractVM.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/05 15:40:20 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/05 15:40:23 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ABSTRACTVM_HPP
# define ABSTRACTVM_HPP

#include "IOperand.hpp"
#include "DataModels.hpp"
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <deque>


class AbstractVM
{
public:
				AbstractVM(void);
				AbstractVM(AbstractVM  const &to_copy);
	AbstractVM	&operator=(AbstractVM const &rhs);
				~AbstractVM();

	void		manageFlags(int argc, char **argv);
	void		printHelp(void);
	void		printInfo(void);
	void		printInstructionsDescription(void);
	void		readInput(int argc, char **argv);
	void		makeLexicalAnalysis();
	void		parseInstructions();
	void		executeInstructions();
	static 		std::map<std::string, std::string> _outputModifiers;

	class AbstractVMException : public std::exception
	{
	public:
				AbstractVMException(void);
				AbstractVMException(std::string error);
				AbstractVMException(AbstractVMException  const &to_copy);
				AbstractVMException &operator=(AbstractVMException const &rhs);
				virtual ~AbstractVMException() throw();
				virtual const char * what() const throw();
	private:
		std::string _errorText;
	};

private:
	typedef void(AbstractVM::*operationMethodPtr)(Instr curInstruction);
	std::vector<IOperand const *> vmStack;	
	void		operationPush(Instr curInstruction);
	void		operationPop(Instr curInstruction);
	void		operationDump(Instr curInstruction);
	void		operationAssert(Instr curInstruction);
	void		operationAdd(Instr curInstruction);
	void		operationSub(Instr curInstruction);
	void		operationMul(Instr curInstruction);
	void		operationDiv(Instr curInstruction);
	void		operationMod(Instr curInstruction);
	void		operationPrint(Instr curInstruction);
	void		operationExit(Instr curInstruction);
	void		operationSort(Instr curInstruction);
	void		operationMin(Instr curInstruction);
	void		operationMax(Instr curInstruction);
	void		operationSwap(Instr curInstruction);
	void		operationSqrt(Instr curInstruction);
	static std::map<eVmInstruction, operationMethodPtr> operationsMethods;
	std::string					_filePath;
	std::vector<std::string>	_rawData;
	std::vector<Lexeme>			_tokens;
	std::vector<Instr>			_instructions;
	std::deque<const IOperand*>	_valuesStack;
};

#endif