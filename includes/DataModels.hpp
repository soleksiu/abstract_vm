// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DataModels.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/05 23:17:27 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/05 23:17:28 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef DATAMODELS_HPP
# define DATAMODELS_HPP

class IOperand;

enum eOperandType
{
	Int8,
	Int16,
	Int32,
	Float,
	Double,
	EOperandTotalItems
};

enum class eVmInstruction {
	Push = 42,
	Pop,
	Dump,
	Assert,
	Add,
	Sub,
	Mul,
	Div,
	Mod,
	Print,
	Exit,
	Sort,
	Min,
	Max,
	Swap,
	Sqrt,
	WrongInstruction = 0
};

struct		Lexeme
{
	std::string	instrName;
	std::string	operand;
};

struct		Instr
{
	eVmInstruction	instruction;
	IOperand const	*operand;
};

#endif