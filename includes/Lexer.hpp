// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Lexer.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/02/27 19:09:32 by soleksiu          #+#    #+#             //
//   Updated: 2019/02/27 19:09:36 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef LEXER_HPP
# define LEXER_HPP
#include <iostream>
#include <vector>
#include "DataModels.hpp"

class Lexer
{
public:
			Lexer(void);
			Lexer(std::vector<Lexeme> _instr);
			Lexer(std::vector<std::string> _inputData);
			Lexer(Lexer  const &to_copy);
	Lexer	&operator=(Lexer const &rhs);
			~Lexer();

	void	setRawData(std::vector<std::string> _inputData);
	void	printDataContent(void) const;
	std::vector<std::string> getRawData() const;
	void	tokenizeInstructions(void);
	std::vector<Lexeme>	getTokens(void) const;

	class LexerException : public std::exception
	{
	public:
				LexerException(void);
				LexerException(std::string error);
				LexerException(LexerException  const &to_copy);
				LexerException &operator=(LexerException const &rhs);
				virtual ~LexerException() throw();
				virtual const char * what() const throw();
	private:
		std::string _errorText;
	};
private:
	std::vector<std::string> 	_rawData;;
	std::vector<Lexeme>			_tokens;
	void		checkOperand(std::string &operandString);
	void		checkInstruction(std::string &instructionString);
	std::string consturctErrorString(std::string errorText);
	static	std::vector<std::string> 	_allErrors;
	static	size_t	lineNb;
	bool			_errorHappened = false;
};

std::ostream &operator<<(std::ostream &out, Lexer const &rhs);

#endif
