// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Parser.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/02/27 19:16:40 by soleksiu          #+#    #+#             //
//   Updated: 2019/02/27 19:16:44 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PARSER_HPP
# define PARSER_HPP
#include <iostream>
#include "DataModels.hpp"
#include "OperandFactory.hpp"
#include <stdexcept>


class Parser
{
public:
			Parser(void);
			Parser(std::vector<Lexeme> lexemes);
			Parser(Parser  const &to_copy);
	Parser	&operator=(Parser const &rhs);
			~Parser();

			std::vector<Instr>	getInstructions(void);
			void				parseInstructions();

	class ParserException: public std::logic_error
	{
	public:
				ParserException(void);
				ParserException(std::string const& msg);
				ParserException(const char* msg);
				ParserException(std::string error);
				ParserException(ParserException  const &to_copy);
				ParserException &operator=(ParserException const &rhs);
				virtual ~ParserException() throw();
				virtual const char * what() const throw();		
	private:
		std::string _errorText;
	};

private:
	static std::map<std::string, eVmInstruction>	_allInstructions;
	static std::map<std::string, eOperandType>		_allTypes;
	std::vector<Instr>	_instructions;
	std::vector<Lexeme> _lexemes;
	OperandFactory		_factory;

	Instr	createInstruction(Lexeme &instrToken);
	void	getInstructionName(Instr &curInstr, std::string &instrStr);
	void	getOperand(Instr &curInstr, std::string &operandStr);
};

std::ostream &operator<<(std::ostream &out, Parser const &rhs);

#endif
