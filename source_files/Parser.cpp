// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Parser.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/02/27 19:17:04 by soleksiu          #+#    #+#             //
//   Updated: 2019/02/27 19:17:09 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "DataModels.hpp"
#include "Parser.hpp"
#include "AbstractVM.hpp"
#include <stdexcept>


std::map<std::string, eVmInstruction> Parser::_allInstructions = 
{
	{"push", eVmInstruction::Push},
	{"pop", eVmInstruction::Pop},
	{"dump", eVmInstruction::Dump},
	{"assert", eVmInstruction::Assert},
	{"add", eVmInstruction::Add},
	{"sub", eVmInstruction::Sub},
	{"mul", eVmInstruction::Mul},
	{"div", eVmInstruction::Div},
	{"mod", eVmInstruction::Mod},
	{"print", eVmInstruction::Print},
	{"sort", eVmInstruction::Sort},
	{"min", eVmInstruction::Min},
	{"max", eVmInstruction::Max},
	{"swap", eVmInstruction::Swap},
	{"sqrt", eVmInstruction::Sqrt},
	{"exit", eVmInstruction::Exit}
};

std::map<std::string, eOperandType> Parser::_allTypes = 
{
	{"int8", Int8},
	{"int16", Int16},
	{"int32", Int32},
	{"float", Float},
	{"double", Double}
};

Parser::Parser(void) {}

Parser::Parser(std::vector<Lexeme> lexemes): _lexemes(lexemes)
{
	parseInstructions();
}

Parser::Parser(Parser  const &to_copy)
{
	*this = to_copy;
}

Parser &Parser::operator=(Parser const &rhs)
{
	if (this != &rhs)
	{
		this->_instructions = rhs._instructions;
	}
	return (*this);
}

Parser::~Parser() {}

void	Parser::parseInstructions() {
	Instr	curInstr;
	bool	exitIsPresent = false;
	for (Lexeme lexeme : this->_lexemes) {
		curInstr = createInstruction(lexeme);
		if (curInstr.instruction == eVmInstruction::Exit) {
			if(this->_instructions.empty())
				throw ParserException("Exit instruction at beginnig of the input");
			if (exitIsPresent)
				throw ParserException("Multiple exit instructions");
			exitIsPresent = true;
		}
		_instructions.push_back(curInstr);
	}
	if (!exitIsPresent)
		throw ParserException("No exit instruction present in the input");
}

Parser::ParserException::ParserException(void): std::logic_error("Error"), _errorText("Parser error"){}

Parser::ParserException::ParserException(std::string const& error) : std::logic_error(error) {
	std::string colorModRed = AbstractVM::_outputModifiers["yellow"];
	std::string colorModWhite = AbstractVM::_outputModifiers["white"];
	std::string boldModifier = AbstractVM::_outputModifiers["bold"];
	std::string resetModifier = AbstractVM::_outputModifiers["reset"];
	this->_errorText = boldModifier + colorModRed + "Parser error: " + colorModWhite + error + resetModifier;
}

Parser::ParserException::ParserException(const char* error) : std::logic_error(error) {
	std::string colorModRed = AbstractVM::_outputModifiers["yellow"];
	std::string colorModWhite = AbstractVM::_outputModifiers["white"];
	std::string boldModifier = AbstractVM::_outputModifiers["bold"];
	std::string resetModifier = AbstractVM::_outputModifiers["reset"];
	this->_errorText = boldModifier + colorModRed + "Parser error: " + colorModWhite + error + resetModifier;
}

Parser::ParserException::ParserException(std::string error) : std::logic_error(error) {
	std::string colorModRed = AbstractVM::_outputModifiers["yellow"];
	std::string colorModWhite = AbstractVM::_outputModifiers["white"];
	std::string boldModifier = AbstractVM::_outputModifiers["bold"];
	std::string resetModifier = AbstractVM::_outputModifiers["reset"];
	this->_errorText = boldModifier + colorModRed + "Parser error: " + colorModWhite + error + resetModifier;
}

Parser::ParserException::ParserException(ParserException  const &to_copy) : std::logic_error(to_copy._errorText)
{
	this->_errorText = to_copy._errorText;
}

Parser::ParserException &Parser::ParserException::operator=(ParserException const &rhs)
{
	if (this != &rhs) {
		this->_errorText = rhs._errorText;
	}
	return (*this);
}

Parser::ParserException::~ParserException()  throw() {}

const char * Parser::ParserException::what() const throw()
{	
	return this->_errorText.c_str();
}

void	Parser::getInstructionName(Instr &curInstr, std::string &instrStr) {
	curInstr.instruction = _allInstructions[instrStr];
}

void	Parser::getOperand(Instr &curInstr, std::string &operandStr) {
	std::string	typeStr;
	std::string	valueStr;
	size_t 		bracketPos;

	if (operandStr.empty()) {
		curInstr.operand = NULL;
	}
	else {
		bracketPos = operandStr.find("(");
		typeStr = operandStr.substr(0, bracketPos);
		valueStr = operandStr.substr(bracketPos + 1, (operandStr.length() - 2 - bracketPos));
		curInstr.operand = _factory.createOperand(_allTypes[typeStr], valueStr);
	}
}

Instr	Parser::createInstruction(Lexeme &instrToken) {
	Instr	curInstr;

 	getInstructionName(curInstr, instrToken.instrName);
	getOperand(curInstr, instrToken.operand);
	return curInstr;
}

std::vector<Instr>	Parser::getInstructions(void) {
		return this->_instructions;
}

std::ostream &operator<<(std::ostream &out, Parser const &rhs)
{
	(void)rhs;
	out <<  "Parser";
	return (out);
}
