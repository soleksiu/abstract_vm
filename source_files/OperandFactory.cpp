// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OperandFactory.cpp                                 :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/01 13:55:37 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/01 13:55:38 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "IOperand.hpp"
#include "OperandFactory.hpp"
#include "Operand.hpp"

OperandFactory::OperandFactory() {
	createMethodsVector.push_back(&OperandFactory::createInt8);
	createMethodsVector.push_back(&OperandFactory::createInt16);
	createMethodsVector.push_back(&OperandFactory::createInt32);
	createMethodsVector.push_back(&OperandFactory::createFloat);
	createMethodsVector.push_back(&OperandFactory::createDouble);
}

OperandFactory::OperandFactory(OperandFactory  const &to_copy) {
	this->createMethodsVector = to_copy.createMethodsVector;
}

OperandFactory &OperandFactory::operator=(OperandFactory const &rhs) {
	if (this != &rhs) {
		this->createMethodsVector = rhs.createMethodsVector;
	}
	return *this;
}

OperandFactory::~OperandFactory() {

}

IOperand const *OperandFactory::createOperand(eOperandType type, std::string const & value ) const {
	auto methodPtr = this->createMethodsVector.at(type);
	IOperand const *operandInstance = (*this.*methodPtr)(value);
	return operandInstance;	
}

IOperand const *OperandFactory::createInt8(std::string const & value) const {
	return new Operand<char>(Int8, value);
}

IOperand const *OperandFactory::createInt16(std::string const & value) const {
	return new Operand<short>(Int16, value);
}

IOperand const *OperandFactory::createInt32(std::string const & value) const {
	return new Operand<int>(Int32, value);
}

IOperand const *OperandFactory::createFloat(std::string const & value) const {
	return new Operand<float>(Float, value);
}

IOperand const *OperandFactory::createDouble(std::string const & value) const {
	return new Operand<double>(Double, value);
}
