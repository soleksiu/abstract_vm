// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/02/27 18:33:42 by soleksiu          #+#    #+#             //
//   Updated: 2019/02/27 18:40:19 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "AbstractVM.hpp"
#include <float.h>

int main(int argc, char **argv)
{
	try {
		AbstractVM vm;
		vm.manageFlags(argc, argv);
		vm.readInput(argc, argv);
		vm.makeLexicalAnalysis();
		vm.parseInstructions();
		vm.executeInstructions();
	}
	catch (std::exception &exc) {
		std::cerr << exc.what() << std::endl;
		return -1;
	}
	catch (...) {
		std::cerr << "Error" << std::endl;
		return -1;
	}
}
