// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AbstractVM.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/03/05 15:40:53 by soleksiu          #+#    #+#             //
//   Updated: 2019/03/05 15:40:54 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <cmath>
#include "IOperand.hpp"
#include "AbstractVM.hpp"
#include "Lexer.hpp"
#include "Parser.hpp"
#include <stdlib.h>

AbstractVM::AbstractVM() {

}

AbstractVM::AbstractVM(AbstractVM  const &to_copy) {
	this->vmStack = to_copy.vmStack;
}

AbstractVM &AbstractVM::operator=(AbstractVM const &rhs) {
	if (this != &rhs) {
		this->vmStack = rhs.vmStack;
	}
	return *this;
}

AbstractVM::~AbstractVM() {

}



void	AbstractVM::printInstructionsDescription(void) {
	std::cout << _outputModifiers["bold"] << "Mandatory instructions\n" << _outputModifiers["reset"]
	<< _outputModifiers["bold"] << "\tpush\t" << _outputModifiers["reset"]
	<<  "Pushes the value v at the top of the stack. Getting operand as argument.\n\n"
	<< _outputModifiers["bold"] << "\tpop\t" << _outputModifiers["reset"]
	<<  "Unstacks the value from the top of the stack.\n"
	<<  "\t\tIf the stack is empty, the program execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tdump\t" << _outputModifiers["reset"]
	<<  "Displays each value of the stack, from the most recent one to the oldest one\n"
	<<  "\t\twithout changing the stack. Each value is separated from the next one by a newline.\n\n"
	<< _outputModifiers["bold"] << "\tassert\t" << _outputModifiers["reset"]
	<<  "Asserts that the value at the top of the stack is equal to the one passed as parameter\n"
	<<  "\t\tfor this instruction. If it is not the case, the execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tadd\t" << _outputModifiers["reset"]
	<<  "Unstacks the first two values on the stack, adds them together and stacks the result.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2, the program execution\n"
	<<  "\t\tmust stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tsub\t" << _outputModifiers["reset"]
	<<  "Unstacks the first two values on the stack, subtracts them, then stacks the result.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2, the program execution\n"
	<<  "\t\tmust stop with an error.\n"
	<< _outputModifiers["bold"] << "\tmul\t" << _outputModifiers["reset"]
	<<  "Unstacks the first two values on the stack, multiplies them together and stacks the result.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2, the program execution\n"
	<<  "\t\tmust stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tdiv\t" << _outputModifiers["reset"]
	<<  "Unstacks the first two values on the stack, divides them together and stacks the result.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2 or the divisor is equal to 0,\n"
	<<  "\t\tthe program execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tmod\t" << _outputModifiers["reset"]
	<<  "Unstacks the first two values on the stack, calculates the modulus and stacks the result.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2 or the divisor is equal to 0,\n"
	<<  "\t\tthe program execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tprint\t" << _outputModifiers["reset"]
	<<  "Asserts that the value at the top of the stack is an 8-bit integer. Then interprets it as an\n"
	<<  "\t\tASCII value and displays the corresponding character on the standard output.\n\n"
	<< _outputModifiers["bold"] << "\texit\t" << _outputModifiers["reset"]
	<<  " Terminate the execution of the current program. If this instruction does not appears while\n"
	<<  "\t\tall thers instruction has been processed, the execution must stop with an error.\n\n";
	std::cout << _outputModifiers["bold"] << "Bonus instructions\n" << _outputModifiers["reset"]
	<< _outputModifiers["bold"] << "\tmin\t" << _outputModifiers["reset"]
	<<  "Finding minimum value, unstacks it and then putting it at the top of the stack.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2, the program execution\n"
	<<  "\t\tmust stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tmax\t" << _outputModifiers["reset"]
	<<  "Finding maximum value, unstacks it and then putting it at the top of the stack.\n"
	<<  "\t\tIf the number of values on the stack is strictly inferior to 2, the program execution\n"
	<<  "\t\tmust stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tswap\t" << _outputModifiers["reset"]
	<<  "Swapping the first two values on the stack. If the number of values on the stack is strictly\n"
	<<  "\t\tinferior to 2, the program execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tsort\t" << _outputModifiers["reset"]
	<<  "Sorting the stack in ascending order. If the number of values on the stack is strictly\n"
	<<  "\t\tinferior to 2, the program execution must stop with an error.\n\n"
	<< _outputModifiers["bold"] << "\tsqrt\t" << _outputModifiers["reset"]
	<<  "Getting the square root of the top of the stack. If the stack is empty or value is negative\n"
	<<  "\t\tnumber, the program execution must stop with an error.\n";
	std::cout << _outputModifiers["bold"] << "Operands description\n" << _outputModifiers["reset"]
	<< _outputModifiers["bold"] << "\tint8(n)   : " << _outputModifiers["reset"]
	<<  "Creates an 8-bit integer with value n.\n"
	<< _outputModifiers["bold"] << "\tint16(n)  : " << _outputModifiers["reset"]
	<<  "Creates an 16-bit integer with value n.\n"
	<< _outputModifiers["bold"] << "\tint32(n)  : " << _outputModifiers["reset"]
	<<  "Creates an 32-bit integer with value n.\n"
	<< _outputModifiers["bold"] << "\tfloat(z)  : " << _outputModifiers["reset"]
	<<  "Creates a float with value z.\n"
	<< _outputModifiers["bold"] << "\tdouble(z) : " << _outputModifiers["reset"]
	<<  "Creates a double with value z.\n"
	<<  "\tWhere n has format: "
	<< _outputModifiers["bold"] << "[-]?[0..9]+" << _outputModifiers["reset"]
	<<  " and z has format: "
	<< _outputModifiers["bold"] << "[-]?[0..9]+.[0..9]+\n" << _outputModifiers["reset"];

	std::exit(0);

}

void	AbstractVM::printHelp(void) {
	std::string	breakingLine = "________________________________________________________________________";
	std::cout << _outputModifiers["cyan"] << _outputModifiers["bold"] 
	<< "\tThis project is meant to create a simple virtual machine\n    that can" 
	<< " interpret programs written in a basic assembly language." 
	<< _outputModifiers["reset"]  <<std::endl;
	std::cout << breakingLine << std::endl;
	std::cout << _outputModifiers["bold"] << 
				 "usage : ./avm [-a | -h | -i] [input instructions using stdin]" << std::endl;
	std::cout << "        ./avm [-a | -h | -i] file_path" << std::endl << _outputModifiers["reset"];
	std::cout << breakingLine << std::endl;
	std::cout << "Available flags description:" << std::endl;
	std::cout << "\t-h, --help\t\tdisplay this help and exit" << std::endl;
	std::cout << "\t-a, --about\t\tdisplay info about project and author" << std::endl;
	std::cout << "\t-i, --instructions\tdisplay vm instructions description " << std::endl;
	std::cout << breakingLine << std::endl;
	std::exit(0);
}

void	AbstractVM::printInfo(void) {
	std::cout << _outputModifiers["bold"] << _outputModifiers["underlined"]
	<< "\t\t\t\t  Abstract VM  \n"
	<< _outputModifiers["reset"]
	<< "\t\t\t\t  C++ Project" << std::endl;
	std::cout << "\t\tThis project is meant to create a simple virtual machine\n\t    that can " 
	<< "interpret programs written in a basic assembly language." << std::endl;
	std::cout << "This is a learning project of 42 school(obviously the best place to code in the Galaxy)." << std::endl;
	std::cout << "\t  Made by "
	<< _outputModifiers["underlined"]
	<< "soleksiu" << _outputModifiers["reset"] << ".student.unit.ua in Unit Factory. Kyiv. Ukraine. 2019." <<  _outputModifiers["reset"] << std::endl;
	std::exit(0);
}


void	AbstractVM::manageFlags(int argc, char **argv) {
	bool multipleFiles = false;
	std::string	curFlag;
	std::string	lowerCased;
	for (int i = 1; i < argc; ++i) {
		curFlag= std::string(argv[i]);
		lowerCased = curFlag;
		std::transform(lowerCased.begin(), lowerCased.end(), lowerCased.begin(), ::tolower);
		if (lowerCased[0] == '-') {
			if(lowerCased == "-h" || lowerCased == "--help")
				AbstractVM::printHelp();
			else if (lowerCased == "-a" || lowerCased == "--about")
				AbstractVM::printInfo();
			else if (lowerCased == "-i" || lowerCased == "--instructions")
				AbstractVM::printInstructionsDescription();
			else 
				throw AbstractVMException("Incorrect AVM launch parameters. Use -h flag for more info.");
		}
		else {
			if (!this->_filePath.empty())
				multipleFiles = true;
			this->_filePath = curFlag;
		}
	}
	if (multipleFiles)
		throw AbstractVMException("Wrong AVM launch parameters. Use -h flag for more info.");

}

void	AbstractVM::readInput(int argc, char **argv) {
	std::string line;
	std::vector<std::string> data = std::vector<std::string >();
	(void)argc;
	if (this->_filePath.empty())
	{
		while (std::getline(std::cin, line))
		{
			if(line.find(";;") != std::string::npos)
				break;
			data.push_back(line);
		}
	}
	// else if (argc == 2) {
	else {
		std::ifstream fileContent(argv[1]);
		if (!fileContent)
			throw AbstractVMException( "Error openning the file" );
		while (std::getline(fileContent, line)) {
			data.push_back(line);
		}
	}
	// else {
	// 	throw AbstractVMException( "Wrong AVM launch parameters." );
	// }
	this->_rawData = data;
}

void	AbstractVM::makeLexicalAnalysis() {
	Lexer	lexer(this->_rawData);
	this->_tokens = lexer.getTokens();

}

void	AbstractVM::parseInstructions() {
	Parser	parser(this->_tokens);
	this->_instructions = parser.getInstructions();
}

void	AbstractVM::executeInstructions() {
	for (auto curInstruction : this->_instructions) {
		auto methodPtr = this->operationsMethods[curInstruction.instruction];
		(*this.*methodPtr)(curInstruction);
	}
}

void	AbstractVM::operationPush(Instr curInstruction) {
	this->_valuesStack.push_front(curInstruction.operand);
}

void	AbstractVM::operationPop(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.empty())
		throw AbstractVMException("Not able to use pop instruction to empty stack");
	this->_valuesStack.pop_front();
}

void	AbstractVM::operationDump(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.empty())
		throw AbstractVMException("Not able to use dump instruction to empty stack");
	for(auto stackValue : _valuesStack) {
		std::cout << stackValue->toString() << std::endl;
	}
}

void	AbstractVM::operationAssert(Instr curInstruction) {
	(void)curInstruction;
	IOperand const *valueToCheck = this->_valuesStack[0];
	if (_valuesStack.empty())
		throw AbstractVMException("Not able to use assert instruction to empty stack");
	if (valueToCheck->getType() != curInstruction.operand->getType())
		throw AbstractVMException("Not able to use assert for different types");
	if (_valuesStack[0]->toString() != curInstruction.operand->toString())
		throw AbstractVMException("Assert operation has false result");
}

void	AbstractVM::operationAdd(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform addition");
	IOperand const *addend1 = this->_valuesStack[0];
	IOperand const *addend2 = this->_valuesStack[1];
	this->_valuesStack.pop_front();
	this->_valuesStack.pop_front();
	IOperand const *result = *addend1 + *addend2;
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationSub(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform subtraction");
	IOperand const *minuend = this->_valuesStack[1];
	IOperand const *subtrahend = this->_valuesStack[0];
	this->_valuesStack.pop_front();
	this->_valuesStack.pop_front();
	IOperand const *result = *minuend - *subtrahend;
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationMul(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform multiplication");
	IOperand const *multiplicand = this->_valuesStack[0];
	IOperand const *multiplier = this->_valuesStack[1];
	this->_valuesStack.pop_front();
	this->_valuesStack.pop_front();
	IOperand const *result = (*multiplicand) * (*multiplier);
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationDiv(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform division");
	IOperand const *quotient = this->_valuesStack[1];
	IOperand const *remainder = this->_valuesStack[0];
	if (!stod(remainder->toString()))
		throw AbstractVMException("Division by zero");
	this->_valuesStack.pop_front();
	this->_valuesStack.pop_front();
	IOperand const *result = *quotient / *remainder;
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationMod(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform division by modulo");
	IOperand const *quotient = this->_valuesStack[1];
	IOperand const *remainder = this->_valuesStack[0];
	if (!stol(remainder->toString()))
		throw AbstractVMException("Modulo division by zero");
	this->_valuesStack.pop_front();
	this->_valuesStack.pop_front();
	IOperand const *result = *quotient % *remainder;
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationPrint(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.empty())
		throw AbstractVMException("Not enough operands to perform print instruction");
	if (_valuesStack[0]->getType() != Int8)
		throw AbstractVMException("Print operation can be performed only for Int8 values");
	char valueToPrint = std::stoi(_valuesStack[0]->toString());
	std::cout << valueToPrint << std::endl;
}

void	AbstractVM::operationSort(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform sort instruction");
	std::sort(this->_valuesStack.begin(), this->_valuesStack.end(), [this](const IOperand *lhs, const IOperand *rhs) {return (*lhs < *rhs);});
}

void	AbstractVM::operationMin(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform min instruction");
	std::deque<const IOperand*>::iterator minValuePt = std::min_element(this->_valuesStack.begin(), this->_valuesStack.end(), [this](const IOperand *lhs, const IOperand *rhs) {return (*lhs < *rhs);});
	this->_valuesStack.push_front(*minValuePt);
	this->_valuesStack.erase(minValuePt);
}

void	AbstractVM::operationMax(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform max instruction");
	std::deque<const IOperand*>::iterator maxValuePt = std::max_element(this->_valuesStack.begin(), this->_valuesStack.end(), [this](const IOperand *lhs, const IOperand *rhs) {return *lhs < *rhs;});
	this->_valuesStack.push_front(*maxValuePt);
	this->_valuesStack.erase(maxValuePt);
}

void	AbstractVM::operationSwap(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.size() < 2)
		throw AbstractVMException("Not enough operands to perform swap instruction");
	std::deque<const IOperand*>::iterator secondValuePt = this->_valuesStack.begin() + 1;
	this->_valuesStack.push_front(*secondValuePt);
	this->_valuesStack.erase(secondValuePt);
}

void	AbstractVM::operationSqrt(Instr curInstruction) {
	(void)curInstruction;
	if (_valuesStack.empty())
		throw AbstractVMException("Not enough operands to perform sqrt instruction");
	IOperand const *topValue = this->_valuesStack[0];
	std::string toCheck;
	toCheck = topValue->toString();
	if (!toCheck.empty() && toCheck[0] == '-')
		throw AbstractVMException("Not able to get sqrt from negative number");
	this->_valuesStack.pop_front();
	OperandFactory factory;
	std::string 	resultStr = std::to_string(std::sqrt(std::stod(topValue->toString())));
	if (topValue->getType() < Float && resultStr[(resultStr.find('.') + 1)] == '0')
		resultStr.erase(resultStr.find('.'), std::string::npos);
	else if (resultStr[(resultStr.find('.') + 1)] == '0')
		resultStr.erase(resultStr.find_last_not_of('0') + 2, std::string::npos);
	else
		resultStr.erase(resultStr.find_last_not_of('0') + 1, std::string::npos);
	IOperand const *result = factory.createOperand(Double, resultStr);
	this->_valuesStack.push_front(result);
}

void	AbstractVM::operationExit(Instr curInstruction) {
	(void)curInstruction;
	// system("leaks avm");
	std::exit(0);
}

typedef void(AbstractVM::*operationMethodPtr)(Instr curInstruction);

std::map<eVmInstruction, operationMethodPtr> AbstractVM::operationsMethods = {
	{eVmInstruction::Push, &AbstractVM::operationPush},
	{eVmInstruction::Pop, &AbstractVM::operationPop},
	{eVmInstruction::Dump, &AbstractVM::operationDump},
	{eVmInstruction::Assert, &AbstractVM::operationAssert},
	{eVmInstruction::Add, &AbstractVM::operationAdd},
	{eVmInstruction::Sub, &AbstractVM::operationSub},
	{eVmInstruction::Mul, &AbstractVM::operationMul},
	{eVmInstruction::Div, &AbstractVM::operationDiv},
	{eVmInstruction::Mod, &AbstractVM::operationMod},
	{eVmInstruction::Print, &AbstractVM::operationPrint},
	{eVmInstruction::Sort, &AbstractVM::operationSort},
	{eVmInstruction::Min, &AbstractVM::operationMin},
	{eVmInstruction::Max, &AbstractVM::operationMax},
	{eVmInstruction::Swap, &AbstractVM::operationSwap},
	{eVmInstruction::Sqrt, &AbstractVM::operationSqrt},
	{eVmInstruction::Exit, &AbstractVM::operationExit}
};

std::map<std::string, std::string>  AbstractVM::_outputModifiers = {
	{"red", "\x1B[31m"},
	{"green", "\x1B[32m"},
	{"blue", "\x1B[34m"},
	{"cyan", "\x1B[36m"},
	{"yellow", "\x1B[33m"},
	{"magenta", "\x1B[35m"},
	{"white","\x1B[37m"},
	{"reset", "\x1B[0m"},
	{"bold", "\033[1m"},
	{"underlined", "\033[4m"},
};


AbstractVM::AbstractVMException::AbstractVMException(void): _errorText("AbstractVM error") {}

AbstractVM::AbstractVMException::AbstractVMException(std::string error) {
	std::string colorModRed = AbstractVM::_outputModifiers["red"];
	std::string colorModWhite = AbstractVM::_outputModifiers["white"];
	std::string boldModifier = AbstractVM::_outputModifiers["bold"];
	std::string resetModifier = AbstractVM::_outputModifiers["reset"];
	this->_errorText = boldModifier + colorModRed + "AbstractVM error: " + colorModWhite + error + resetModifier;
}

AbstractVM::AbstractVMException::AbstractVMException(AbstractVMException  const &to_copy) 
{
	this->_errorText = to_copy._errorText;
}

AbstractVM::AbstractVMException &AbstractVM::AbstractVMException::operator=(AbstractVMException const &rhs)
{
	if (this != &rhs) {
		this->_errorText = rhs._errorText;
	}
	return (*this);
}

AbstractVM::AbstractVMException::~AbstractVMException()  throw() {}

const char * AbstractVM::AbstractVMException::what() const throw()
{
	return this->_errorText.c_str();
}

