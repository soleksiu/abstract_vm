// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Lexer.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: soleksiu <soleksiu@student.unit.ua>        +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/02/27 19:10:09 by soleksiu          #+#    #+#             //
//   Updated: 2019/02/27 19:10:19 by soleksiu         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <sstream>
#include <regex>
#include "Lexer.hpp"
#include "AbstractVM.hpp"

Lexer::Lexer(void) : _tokens() {}

Lexer::Lexer(std::vector<Lexeme> tokens) : _tokens(tokens) 
{
}

Lexer::Lexer(std::vector<std::string> _inputData): _rawData(_inputData)
{
	tokenizeInstructions();
}

Lexer::Lexer(Lexer  const &to_copy)
{
	this->_tokens = to_copy._tokens;
}

Lexer &Lexer::operator=(Lexer const &rhs)
{
	if (this != &rhs)
	{
		this->_tokens = rhs._tokens;
	}
	return (*this);
}

Lexer::~Lexer() {}

void	Lexer::setRawData(std::vector<std::string> _inputData) {
	this->_rawData = _inputData;
}

std::vector<std::string> Lexer::getRawData() const {
	return this->_rawData;
}

std::string Lexer::consturctErrorString(std::string errorText) {
	std::string colorModRed = AbstractVM::_outputModifiers["blue"];
	std::string colorModWhite = AbstractVM::_outputModifiers["white"];
	std::string boldModifier = AbstractVM::_outputModifiers["bold"];
	std::string resetModifier = AbstractVM::_outputModifiers["reset"];
	std::string tail = boldModifier + colorModRed + "Lexer error: " + colorModWhite + "line#" + std::to_string(Lexer::lineNb) + ": ";
	this->_errorHappened = true;
	return tail + errorText + resetModifier + "\n";
}

void	Lexer::checkOperand(std::string &operandString) {
	std::regex	operandTemplate {R"((((int8|int16|int32)(\([-]?\d+\))))|((float|double)(\([-]?\d+\.\d+\))))"};
	std::smatch	matches;
	if (!(regex_match(operandString, matches, operandTemplate)))
		this->_allErrors.push_back(consturctErrorString("Wrong operand data"));
}

void	Lexer::checkInstruction(std::string &instructionString) {
	std::regex	instructionTemplate {R"(push|pop|dump|assert|add|sub|mul|div|mod|print|sort|min|max|swap|sqrt|exit)"};
	std::smatch	matches;
	if (!(regex_match(instructionString, matches, instructionTemplate)))
		this->_allErrors.push_back(consturctErrorString("Wrong instruction"));
}

void	Lexer::tokenizeInstructions(void) {
	std::string			firstLexeme;
	std::string			secondLexeme;
	std::string			thirdLexeme;
	for (size_t i = 0; i < this->_rawData.size(); ++i) {
		this->lineNb = i + 1;
		std::string curStr = _rawData[i];
		firstLexeme.erase();
		secondLexeme.erase();
		thirdLexeme.erase();
		if (curStr.find(";") != std::string::npos) {
			curStr = curStr.erase(curStr.find(";"), std::string::npos);
		}
		std::istringstream	tmpStream(curStr);
		Lexeme				tmpToken;
		tmpStream >> firstLexeme >> secondLexeme >> thirdLexeme;
		if (firstLexeme.empty() && secondLexeme.empty() && thirdLexeme.empty())
			continue;
		if (!thirdLexeme.empty())
			this->_allErrors.push_back(consturctErrorString("To many aguments in instruction"));
		else if (!secondLexeme.empty()) {
			checkInstruction(firstLexeme);
			checkOperand(secondLexeme);
			std::regex	instructionTemplate {R"(pop|dump|add|sub|mul|div|mod|print|sort|min|max|swap|sqrt|exit)"};
			std::smatch	matches;
			if ((regex_match(firstLexeme, matches, instructionTemplate)))
				this->_allErrors.push_back(consturctErrorString("Operand for " + firstLexeme + " command"));
		}
		else if (!firstLexeme.empty()) {
			checkInstruction(firstLexeme);
			if (firstLexeme == "push" || firstLexeme == "assert")
				this->_allErrors.push_back(consturctErrorString("No operand for " + firstLexeme + " command"));
		}
		tmpToken.instrName = firstLexeme;
		tmpToken.operand = secondLexeme;
		this->_tokens.push_back(tmpToken);
	}
	if (this->_errorHappened) {
		std::string	finalString;
		for (std::string errorString : this->_allErrors) {
			finalString += errorString;
		}
		finalString.pop_back();
		throw LexerException(finalString);
	}

}

std::vector<Lexeme>	Lexer::getTokens(void) const {
	return this->_tokens;
}

Lexer::LexerException::LexerException(void): _errorText("Lexer error") {}

Lexer::LexerException::LexerException(std::string error): _errorText(error) {}

Lexer::LexerException::LexerException(LexerException  const &to_copy) 
{
	this->_errorText = to_copy._errorText;
}

Lexer::LexerException &Lexer::LexerException::operator=(LexerException const &rhs)
{
	if (this != &rhs) {
		this->_errorText = rhs._errorText;
	}
	return (*this);
}

Lexer::LexerException::~LexerException()  throw() {}

const char * Lexer::LexerException::what() const throw()
{
	return this->_errorText.c_str();
}

std::ostream &operator<<(std::ostream &out, Lexer const &rhs)
{
	std::vector<std::string> data = rhs.getRawData();
	for (size_t i = 0; i < data.size(); ++i) {
		out << data[i] << std::endl;
	}
	return (out);
}


size_t Lexer::lineNb = 0;
std::vector<std::string> Lexer::_allErrors;